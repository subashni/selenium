# Sage People Automation Testing

## Overview

Sage people is a cloud-based people HR product.One of the main features of HR workflow is  Performance management. This automation project contains all the necessary page objects required to create new objective.

## Configuration And Dependencies
This is a maven based project with following common Dependencies
   * Cucumber
   * Testng
   * Selenium
   
## Local Build and Run
To build the project ,pull the code from <https://subashni@bitbucket.org/subashni/selenium.git> 

## Maven Commands

Installing

mvn clean install
Will build and install this project into local repository and run all the testng test.

mvn clean install -DskipTests
Will build and install this project into local repository without running any test.

To Run Tests

mvn -Dtest -Dregression_suit.xml
Will run only the tests which are part of the testng xml.The login credentials and url will be read from the config.properties file.

mvn -Dtest -Dregression_suit.xml -Dapp.url=<url> -Duser.email=<emailaddress> -Duser.password=<>
Will run the test based on the credentials password as environment variables.

