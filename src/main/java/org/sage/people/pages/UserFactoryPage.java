package org.sage.people.pages;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sage.people.base.TestBase;
import org.sage.people.utils.SeleniumUtils;
/**
 * As the user core navigation area. This is common for all the pages
 * @author sprasanna
 *
 */
public class UserFactoryPage extends TestBase
{
   private static Log logger = LogFactory.getLog(UserFactoryPage.class);
   @FindBy(id="stacked-menu")
    WebElement elementStackMenu;
   
   @FindBy( id="wx-header__hamburger")
   WebElement elementNav;
    
    public UserFactoryPage() 
    {
        PageFactory.initElements(getDriver(), this);
    }
    
    public void UserNavActionRender()
    {
         SeleniumUtils.render(elementStackMenu);     
    }
    /**
     * Click on the side bar navigation control
     */
    
    public void navigationToUserAction()
    {
        logger.info("select the side bar navigation control");
        elementNav.click();
    }
    /**
     * This is common utility control which will help to access all the useroptions
     * @param options
     * @throws Exception
     */
    public void selectOptions(UserOptions options) throws Exception
    {
        logger.info("user option to select " + options.getOtions());
        WebElement element = SeleniumUtils.waitUntilElementIsVisibleById("stacked-menu");
        WebElement itemToSelect = SeleniumUtils.findItem(element, options.getOtions());
        itemToSelect.click();
    }
    /**
     * This is common utility control which will help to access all the suboptions once the option is selected
     * @param options
     * @throws Exception
     */
    
    public void selectSuboption(String subOption) throws Exception
    {
        logger.info("user sub option to select " + subOption);
        WebElement element = SeleniumUtils.waitUntilElementIsVisibleById("item-user-4");
        List<WebElement> navigationItem = element.findElements(By.tagName("li"));
        for(WebElement each : navigationItem)
        {
            if(each.getText().toLowerCase().equalsIgnoreCase(subOption))
            {
                System.out.println("each " + each.getText());
                each.findElement(By.tagName("a")).click();
            }
        }
    }
}
