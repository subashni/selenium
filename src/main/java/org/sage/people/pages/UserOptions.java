package org.sage.people.pages;

/**
 * This is a enum method which will contain all the possible userOptions
 * 
 * @author sprasanna
 */
public enum UserOptions
{
    COMMUNICATIONS("Communications"), PERFORMANCE("Performance"), PERSONALDETAILS("Personal Details"), PAYANDBENEFITs("Pay and Benfits");

    private String options;

    private UserOptions(String options)
    {
        this.options = options;
    }

    public String getOtions()
    {
        return options;
    }

}
