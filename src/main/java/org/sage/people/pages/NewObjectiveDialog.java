package org.sage.people.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sage.people.base.TestBase;
import org.sage.people.utils.SeleniumUtils;

public class NewObjectiveDialog extends TestBase
{
    @FindBy(xpath="//input[@ng-model='data.name']")
    WebElement elementObjectiveName;
    
    @FindBy(xpath="//textarea[@ng-model='data.description']")
    WebElement elementDescription;
    
    @FindBy(xpath="//textarea[@ng-model='data.measure']")
    WebElement elementMesaure;
    
    @FindBy(xpath="//datepicker-directive[@ng-model='data.startDate']/div/label/input")
    WebElement elementStartDate;
    
    @FindBy(xpath="//datepicker-directive[@ng-model='data.endDate']/div/label/input")
    WebElement elementEndate;
    
    @FindBy(xpath="//datepicker-directive[@ng-model='data.nextReviewDate']/div/label/input")
    WebElement elementReviewDate;
    
    @FindBy(css="button.btn:nth-child(6)")
    WebElement elementSave;
    
    public NewObjectiveDialog() 
    {
        PageFactory.initElements(getDriver(), this);
        getDriver().manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    
    public void enterObjectiveName(String name)
    {
        SeleniumUtils.waitUntilElementVisible(elementObjectiveName);
        elementObjectiveName.sendKeys(name);
    }

    public void enterObjectiveDescription(String name)
    {
        SeleniumUtils.waitUntilElementVisible(elementDescription);
        elementDescription.sendKeys(name);
    }
    
    public void enterMeasure(String name)
    {
        elementMesaure.sendKeys(name);
    }
    
    public void selectStartDate(String startDate)
    {
        elementStartDate.clear();
        elementStartDate.sendKeys(startDate);
    }
    
    public void selectEndDate(String endDate)
    {
        elementEndate.clear();
        elementEndate.sendKeys(endDate);
        elementEndate.sendKeys(Keys.ENTER);
    }
    
    public void selectReviewDate(String endDate) throws Exception
    {
        Thread.sleep(1000);
        elementReviewDate.clear();
        elementReviewDate.sendKeys(endDate);
        elementReviewDate.sendKeys(Keys.ENTER);
    }
    
    public ConfirmationDialog selectReveal()
    {
        elementSave.click();
        return new ConfirmationDialog();
    }
    
    public String getNewObjectiveUserName()
    {
        WebElement name = getDriver().findElement(By.xpath("//h4[@ng-hide='data.id']"));
        return (name.getText());
    }
}

