package org.sage.people.pages;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sage.people.utils.SeleniumUtils;

public class PerformancePage extends UserFactoryPage
{
    private static Log logger = LogFactory.getLog(PerformancePage.class);
    @FindBy(xpath="/html/body/div/section[2]/div/ui-view/div[2]/div[2]/div/div/div")
    WebElement elementPeformanceTable;
    
    public PerformancePage() 
    {
        PageFactory.initElements(getDriver(), this);
    }

    public void renderPerformancePage()
    {
       SeleniumUtils.render(elementPeformanceTable);
    }
    /**
     * is the page loaded correctly and table is displayed
     * @return
     */
    public boolean isPerformancePageDisplayed()
    {
        logger.info("is performance table displayed correct");
        return elementPeformanceTable.isDisplayed();
    }
    /**
     * Click on the sub section this is controlled based on the main page selected.
     * @param subOption
     * @return
     * @throws Exception
     */
   
    public ObjectivePage selectSubOption(String subOption) throws Exception
     {
            UserFactoryPage navigate = new UserFactoryPage();
            navigate.navigationToUserAction();
            navigate.UserNavActionRender();
            navigate.selectSuboption(subOption);
            return new ObjectivePage();
    }
    
}
