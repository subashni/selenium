package org.sage.people.pages;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sage.people.base.TestBase;

/**
 * All the methods required for login page
 * 
 * @author sprasanna
 */
public class LoginPage extends TestBase
{
    private static Log logger = LogFactory.getLog(LoginPage.class);
    @FindBy(id = "username")
    WebElement elementUserName;

    @FindBy(id = "password")
    WebElement elementPassword;

    @FindBy(id = "Login")
    WebElement elementLoginBtn;

    @FindBy(id = "error")
    WebElement elementError;

    @FindBy(id = "logo")
    WebElement elementLogo;

    public LoginPage()
    {
        PageFactory.initElements(getDriver(), this);
    }

    /**
     * Gets the title of the login page
     * @return String
     */

    public String getLoginPageTitle()
    {
        logger.info("get title");
        return getDriver().getTitle();
    }

    /**
     * Enter User Name
     * @param userName - String
     */
    public void enterUserName(String userName)
    {
        logger.info("Enter UserName");
        elementUserName.clear();
        elementUserName.sendKeys(userName);
    }

    /**
     * Enter password
     * @param password
     */

    public void enterPassword(String password)
    {
        logger.info("Enter Password");
        elementPassword.clear();
        elementPassword.sendKeys(password);
    }

    /**
     * Click on Login button
     * @return - HomePage
     */
    public HomePage clickLogin()
    {
        logger.info("Click Login Button");
        elementLoginBtn.click();
        return new HomePage();
    }

    /**
     * Get Error text in login screen
     * @return
     */
    public String getErrorText()
    {
        logger.info("Get error text to compare");
        return elementError.getText();
    }

}
