package org.sage.people.pages;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sage.people.utils.SeleniumUtils;
/**
 * This page contains all the methods that are performed by the team member
 * This page is act like main landing page for the user.
 * @author sprasanna
 *
 */
public class UserPage extends UserFactoryPage
{
    private static Log logger = LogFactory.getLog(UserPage.class);
    @FindBy(xpath="/html/body/div/section[2]/div/ui-view/div[2]/div[2]/div/div[1]/div/div/a/div[2]/h4")
    WebElement elementUserPage;
  
    public UserPage()
    {
        PageFactory.initElements(getDriver(), this);
    }
    /**
     * render page
     */
    public void userPageRender()
    {
        SeleniumUtils.render(elementUserPage);
    }
    
    /**
     * is page displayed
     */
    public boolean isUserPageDisplayed()
    {
        return elementUserPage.isDisplayed();
    }
    
    /**
     * From the user page the user is selecting a option by using the navigation control
     * The options to select is passed as enum
     * @param options
     * @return
     * @throws Exception
     */
  
    public PerformancePage selectUserOption(UserOptions options) throws Exception
    {
        logger.info("The user click on the navigation and select " + options.getOtions() +" as choice");
        UserFactoryPage navigate = new UserFactoryPage();
        navigate.navigationToUserAction();
        navigate.UserNavActionRender();
        navigate.selectOptions(options);
        logger.info("go to performance page");
        return new PerformancePage();
    }
    
}