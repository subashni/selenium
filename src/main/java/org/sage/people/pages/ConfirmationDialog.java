package org.sage.people.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ConfirmationDialog extends UserFactoryPage
{
   @FindBy(xpath="//div[@class='col-lg-12 confirm-action-btn']/button")
   WebElement elementOk;
   
   public ConfirmationDialog()
   {
       PageFactory.initElements(getDriver(), this);
   }
   public void selectOk()
   {
       elementOk.click();
   }
}
