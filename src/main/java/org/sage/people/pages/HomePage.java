package org.sage.people.pages;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sage.people.base.TestBase;
import org.sage.people.utils.SeleniumUtils;

/**
 * HomePage is the first screen the user will see after login screen
 * This contains the navigation option for all the user screens.
 * 
 * @author sprasanna
 */
public class HomePage extends TestBase
{
    private static Log logger = LogFactory.getLog(HomePage.class);

    @FindBy(className = "currentStatusUserName")
    WebElement elementCurrentUserName;

    @FindBy(id = "01r0N000000dDUb_Tab")
    WebElement elementWorkForce;

    public HomePage()
    {
        PageFactory.initElements(getDriver(), this);
    }

    /**
     * Method to get welcome message which will return the user name details.
     * @return - String the username details
     */
    public String getWelcomeMessage()
    {
        logger.info("Validate whether the user is logged in successful by checking the user is in home page");
        return elementCurrentUserName.getText();
    }

    /**
     * A render page will wait until the important element is present in the page.
     * This method is mainly used as sometime certain pages loading will take time
     * In order to be very sure the page is loaded correctly then can render which will
     * wait for a particular element to appear.
     */
    public void renderHomePage()
    {
        logger.info("wait until the home page is render");
        SeleniumUtils.render(elementCurrentUserName);

    }

    /**
     * Method to select the workforce in the homepage
     * @return - UserPage
     */
    public UserPage selectWorkForce()
    {
        elementWorkForce.click();
        return new UserPage();
    }

}
