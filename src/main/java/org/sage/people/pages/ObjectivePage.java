package org.sage.people.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.sage.people.utils.SeleniumUtils;
/**
 * This is objective page selected from performance page 
 * @author sprasanna
 *
 */
public class ObjectivePage extends UserFactoryPage
{
    @FindBy(className="wx-buttons__container")
    WebElement elementCurrentObjectiveHeader;
    
    @FindBy(xpath="//wx-button-open[@operation-id='create']/button")
    WebElement elementBtnNew;
    
    public ObjectivePage()
    {
        PageFactory.initElements(getDriver(), this);
    }
    
    public void objectivePageRender()
    {
        SeleniumUtils.render(elementCurrentObjectiveHeader);
    }
    /**
     * validate whether the objective page is laoded correctly 
     * @return
     */
    public boolean isObjectivePageDisplayed()
    {
        return elementCurrentObjectiveHeader.isDisplayed();
    }
    /**
     * select new in the objective page to create a new objective
     * @return - NewObjectiveDialog
     */
    public NewObjectiveDialog selectNewObejective()
    {
        elementBtnNew.click();
        return new NewObjectiveDialog();
    }           
}
