package org.sage.people.utils;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sage.people.base.TestBase;

/**
 * All the utils required for selenium action are present here
 * 
 * @author sprasanna
 */
public class SeleniumUtils extends TestBase
{
    /**
     * Common render logic for a particular element
     * 
     * @param elementToCheck
     */
    public static void render(WebElement elementToCheck)
    {
        if (elementToCheck == null)
        {
            throw new IllegalArgumentException("WebElement is missing");
        }
        WebDriverWait wait = new WebDriverWait(getDriver(), getTimeOutInSeconds());
        wait.until(ExpectedConditions.visibilityOf(elementToCheck));
    }

    public static void render(WebElement elementToCheck, long timeOutInSeconds)
    {
        if (elementToCheck == null)
        {
            throw new IllegalArgumentException("WebElement is missing");
        }
        WebDriverWait wait = new WebDriverWait(getDriver(), timeOutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(elementToCheck));
    }

    /**
     * Validate whether the element is visible by id
     * 
     * @param id
     * @return
     */

    public static WebElement waitUntilElementIsVisibleById(String id)
    {
        if (id == null)
        {
            throw new IllegalArgumentException("WebElement is missing");
        }
        WebDriverWait wait = new WebDriverWait(getDriver(), getTimeOutInSeconds());
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id(id)));
        return element;
    }

    /**
     * validate element is visible
     * 
     * @param id
     * @return
     */

    public static void waitUntilElementVisible(WebElement element)
    {
        WebDriverWait wait = new WebDriverWait(getDriver(), getTimeOutInSeconds());
        wait.until(ExpectedConditions.visibilityOf(element));

    }

    /**
     * Find a particular element
     */
    public static WebElement findItem(WebElement element, String option)
    {
        List<WebElement> navigationItem = element.findElements(By.tagName("li"));
        for (WebElement each : navigationItem)
        {
            if (each.getText().contains(option))
            {
                return each;
            }
        }

        return null;
    }
}
