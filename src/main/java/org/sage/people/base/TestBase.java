package org.sage.people.base;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


/**
 * This class reads the config.properties file and initialise the browser for
 * the page object
 * 
 * @author sprasanna
 */
public class TestBase
{
    private static Log logger= LogFactory.getLog(TestBase.class); 
    private static WebDriver driver;
    private String loginEmail;
    private String password;
    private String url;
    private static final String USERNAMEPROPERTY = "user.email";
    private static final String PASSWORDPROPERTY = "user.password";
    private static final String URLPROPERTY = "app.url";
    private Properties prop ;
    private static long timeOutInSeconds=10;
       /**
     * This method will start the driver and read the values of the properties that needs to be used by the test.
     * 
     * @throws Exception
     */
    public void initialization() throws Exception
    {
        logger.info("Initialize Common things required for test");
        prop = new Properties();
        prop.load(this.getClass().getClassLoader().getResourceAsStream("config.properties"));
        Path path = FileSystems.getDefault().getPath(System.getProperty("user.dir") + "/src/main/resources/geckodriver");
        System.setProperty("webdriver.gecko.driver", path.toString());
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        getPropertyValues();
    }
    /**
     * Read the properties from the file... If the value is not set in the property file then read it from system properties.
     * for example if we pass values as maven arguments it will be read as system properties. 
     * @throws Exception
     */
    private void getPropertyValues() throws Exception
    {
      
        url = prop.getProperty(URLPROPERTY);
        if (url.isEmpty())
        {
            url = System.getProperty(URLPROPERTY);
        }

        loginEmail = prop.getProperty(USERNAMEPROPERTY);
        if (loginEmail.isEmpty())
        {
            loginEmail = System.getProperty(USERNAMEPROPERTY);
        }
        password = prop.getProperty(PASSWORDPROPERTY);
        if (password.isEmpty())
        {
            password = System.getProperty(PASSWORDPROPERTY);
        }

    }

    public String getLoginEmail()
    {
        return loginEmail;
    }

    public String getPassword()
    {
        return password;
    }

    public String getUrl()
    {
        return url;
    }

    public static WebDriver getDriver()
    {
        return driver;
    }
    public static long getTimeOutInSeconds()
    {
        return timeOutInSeconds;
    }

}
