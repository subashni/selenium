package org.sage.people.regressiontest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sage.people.pages.HomePage;
import org.sage.people.pages.LoginPage;
import org.sage.people.utils.TestAbstract;
import org.testng.Assert;
import org.testng.annotations.Test;
/**
 * This class all the test related to login page both postive and negative case 
 * @author sprasanna
 *
 */
public class LoginTest extends TestAbstract
{
    private static Log logger= LogFactory.getLog(LoginTest.class); 
    LoginPage loginPage;
    /**
     * Test to validate invalid login details 
     */
    @Test(priority = 0)
    public void invalidLoginDetails() throws Exception
    {
        logger.info("In the login page enter invalid details");
        loginPage = new LoginPage(); 
        loginSteps("test@test.com","test");
        Assert.assertTrue(loginPage.getErrorText().contains("Your access to salesforce.com has been disabled by your System Administrator"), String.format("error message does not match"));
    }
    /**
     * Valid login test
     * @throws Exception
     */
    @Test(priority = 1)
    public void validLoginDetails() throws Exception
    {
        logger.info("In the login page enter valid details");
        loginPage = new LoginPage();
        loginSteps(getLoginEmail(),getPassword());
        HomePage homePage = new HomePage() ;
        homePage.renderHomePage();
        Assert.assertTrue(homePage.getWelcomeMessage().contains(getUserName()), String.format("The login details does not match [%s]", homePage.getWelcomeMessage()));
    }
    /**
     * login action method 
     * @param email
     * @param password
     */
    
    private void loginSteps(String email , String password)
    {
        loginPage.enterUserName(email);
        loginPage.enterPassword(password);
        loginPage.clickLogin();
    }
}
