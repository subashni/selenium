package org.sage.people.regressiontest;

import org.sage.people.pages.ConfirmationDialog;
import org.sage.people.pages.NewObjectiveDialog;
import org.sage.people.pages.ObjectivePage;
import org.sage.people.testdata.Objective;
import org.sage.people.utils.TestAbstract;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * @author sprasanna
 */
public class NewObjectiveTest extends TestAbstract
{
    NewObjectiveDialog newObjectiveDialog;

    @Test(priority = 0)
    public void selectNewObjectiveTest() throws Exception
    {
        ObjectivePage objectivePage = new ObjectivePage();
        objectivePage.objectivePageRender();
        newObjectiveDialog = objectivePage.selectNewObejective();
        System.out.println("getUserName()" + getUserName());
        System.out.println(newObjectiveDialog.getNewObjectiveUserName());
        Assert.assertTrue(newObjectiveDialog.getNewObjectiveUserName().contains(getUserName()),
                String.format("The names are not matching, the page name is [%s]", newObjectiveDialog.getNewObjectiveUserName()));
    }
    /**
     * Create new objective test and validate objective page is returned
     * @throws Exception
     */
    
    @Test(priority =1)
    public void enterNewObjectiveAndSave() throws Exception
    {
        Objective objectiveData = new Objective();
        objectiveData.setObjectiveData();
        newObjectiveDialog.enterObjectiveName(objectiveData.getObjectiveName());
        newObjectiveDialog.enterObjectiveDescription(objectiveData.getObjectiveDesc());
        newObjectiveDialog.enterMeasure(objectiveData.getObjectiveMeasure());
        newObjectiveDialog.selectStartDate(objectiveData.getObjectiveStartDate());
        newObjectiveDialog.selectEndDate(objectiveData.getObjectiveEndDate());
        newObjectiveDialog.selectReviewDate(objectiveData.getObjectiveReviewDate());
        ConfirmationDialog confirm = newObjectiveDialog.selectReveal();
        confirm.selectOk();
        ObjectivePage objectivePage = new ObjectivePage();
        objectivePage.objectivePageRender();
        Assert.assertTrue(objectivePage.isObjectivePageDisplayed());
    }
}
