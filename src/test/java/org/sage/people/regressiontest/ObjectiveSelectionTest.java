package org.sage.people.regressiontest;

import org.sage.people.pages.ObjectivePage;
import org.sage.people.pages.PerformancePage;
import org.sage.people.pages.UserOptions;
import org.sage.people.pages.UserPage;
import org.sage.people.utils.TestAbstract;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ObjectiveSelectionTest extends TestAbstract
{
    /**
     * Test to validate whether performance page is displayed
     */
    PerformancePage performancePage ;
    @Test(priority=0)
    public void selectPerformanceTest() throws Exception
    {
        UserPage userPage= new UserPage();
        userPage.userPageRender();
        performancePage = userPage.selectUserOption(UserOptions.PERFORMANCE);
        performancePage.renderPerformancePage();
        Assert.assertTrue((performancePage.isPerformancePageDisplayed()), String.format("The performance page is not displayed correctly"));
    }
    /**
     * Test to select objective under performance 
     * @throws Exception
     */
    
    @Test(priority =1)
    public void selectSubOptionObjectiveTest() throws Exception
    {
        ObjectivePage objectivePage = performancePage.selectSubOption("Objectives");
        objectivePage.objectivePageRender();
        Assert.assertTrue((objectivePage.isObjectivePageDisplayed()),String.format("The objective page is not displayed"));
    }
}
