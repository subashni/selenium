package org.sage.people.regressiontest;

import org.sage.people.pages.HomePage;
import org.sage.people.pages.UserPage;
import org.sage.people.utils.TestAbstract;
import org.testng.Assert;
import org.testng.annotations.Test;
/**
 * Test class contains all the validation to access Home Page
 * @author sprasanna
 *
 */
public class HomePageTest extends TestAbstract
{
    HomePage homePage;
   /**
    * Test to validate home page is displayed
    * @throws Exception
    */
    @Test()
    public void selectWorkForce() throws Exception
    {
        homePage = new HomePage();
        homePage.renderHomePage();
        UserPage userPage = homePage.selectWorkForce();
        userPage.userPageRender();
        Assert.assertTrue(userPage.isUserPageDisplayed());
        
    }
}
