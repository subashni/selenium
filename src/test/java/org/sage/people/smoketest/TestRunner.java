package org.sage.people.smoketest;

import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
/**
 * Cumcumber test runner method
 * @author sprasanna
 *
 */
@CucumberOptions(
        features = {"src/test/java/org/sage/people/feature"}, 
        glue={"org.sage.people.stepdefinitions"}, 
        format= {"pretty","html:test-output"}, 
        monochrome = true, 
        strict = true, 
        dryRun = false 
        )

public class TestRunner extends AbstractTestNGCucumberTests
{  
}