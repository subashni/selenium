package org.sage.people.stepdefinitions;

import org.sage.people.base.TestBase;
import org.sage.people.pages.HomePage;
import org.sage.people.pages.LoginPage;
import org.sage.people.utils.TestAbstract;
import org.testng.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
/**
 * Step Definition file which contains all the methods required for login tests
 * @author sprasanna
 *
 */
@SuppressWarnings("static-access")
public class LoginStepDefintions
{
    TestBase base = new TestBase();
    LoginPage loginPage;
   
    
    
    @Given("^user is in the salesforce login page$")
    public void userLoginIn() throws Exception
    {
        base.initialization();
        base.getDriver().get(base.getUrl());
        loginPage = new LoginPage(); 
        Assert.assertTrue(loginPage.getLoginPageTitle().contains("Login"), String.format("The login page [%s] title does not match", loginPage.getLoginPageTitle()));
    }
    
    @Then("^user enters \"(.*)\" and \"(.*)\"$")
    public void userEnterLoginDetails(String userName, String password)
    {
        loginPage.enterUserName(userName);
        loginPage.enterPassword(password);
    }
    
    @Then("^user enters username and password$")
    public void userEnterLoginDetails()
    {
        loginPage.enterUserName(base.getLoginEmail());
        loginPage.enterPassword(base.getPassword());
    }
    
    @Then("^user click on login button$")
    public void clickOnLoginButton()
    {
        loginPage.clickLogin();
    }
    
    @Then("^validate error message is displayed$")
    public void validateErrorMessage()
    {
        String erorText = loginPage.getErrorText();
        Assert.assertTrue(erorText.contains("Your access to salesforce.com has been disabled by your System Administrator"), String.format("error message does not match"));
    }

    @Then("^quit browser$")
    public void quit_browser()
    {
        base.getDriver().quit();
    }
    
    @Then("^validate is successful logged in$")
    public void validateSuccessfulLogin() throws Exception
    {
        TestAbstract util = new TestAbstract();
        util.readProperties();
        HomePage homePage = new HomePage() ;
        homePage.renderHomePage();
        Assert.assertTrue(homePage.getWelcomeMessage().contains(util.getUserName()), String.format("The login details does not match [%s]", homePage.getWelcomeMessage()));
    }
}
