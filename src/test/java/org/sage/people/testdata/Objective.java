package org.sage.people.testdata;

import org.sage.people.utils.TestAbstract;

public class Objective extends TestAbstract
{
    private String objectiveName;
    private String objectiveDesc;
    private String objectiveMeasure;
    private String objectiveStartDate;
    private String objectiveEndDate;
    private String objectiveReviewDate;

    public String getObjectiveName()
    {
        return objectiveName;
    }

    public void setObjectiveName(String objectiveName)
    {
        this.objectiveName = objectiveName;
    }

    public String getObjectiveDesc()
    {
        return objectiveDesc;
    }

    public void setObjectiveDesc(String objectiveDesc)
    {
        this.objectiveDesc = objectiveDesc;
    }

    public String getObjectiveMeasure()
    {
        return objectiveMeasure;
    }

    public void setObjectiveMeasure(String objectiveMeasure)
    {
        this.objectiveMeasure = objectiveMeasure;
    }

    public String getObjectiveStartDate()
    {
        return objectiveStartDate;
    }

    public void setObjectiveStartDate(String objectiveStartDate)
    {
        this.objectiveStartDate = objectiveStartDate;
    }

    public String getObjectiveEndDate()
    {
        return objectiveEndDate;
    }

    public void setObjectiveEndDate(String objectiveEndDate)
    {
        this.objectiveEndDate = objectiveEndDate;
    }

    public String getObjectiveReviewDate()
    {
        return objectiveReviewDate;
    }

    public void setObjectiveReviewDate(String objectiveReviewDate)
    {
        this.objectiveReviewDate = objectiveReviewDate;
    }

  

    public void setObjectiveData()
    {
        setObjectiveName(prop.getProperty("objective.name"));
        setObjectiveDesc(prop.getProperty("objective.desc"));
        setObjectiveMeasure(prop.getProperty("objective.measure"));
        setObjectiveStartDate(prop.getProperty("objective.startDate"));
        setObjectiveEndDate(prop.getProperty("objective.endDate"));
        setObjectiveReviewDate(prop.getProperty("Objective.reviewDate"));
    }
}
