package org.sage.people.utils;

import java.lang.reflect.Method;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.sage.people.base.TestBase;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

/**
 * Contains all the abstract details required to create a test
 * 
 * @author sprasanna
 */
@SuppressWarnings("static-access")
public class TestAbstract extends TestBase
{
    public static String userName;
    protected static Properties prop;
    private static Log logger = LogFactory.getLog(TestAbstract.class);

    public void readProperties() throws Exception
    {
        logger.info("Initialize Common things required for test");
        prop = new Properties();
        prop.load(this.getClass().getClassLoader().getResourceAsStream("testobjective.properties"));
        setUserName(prop.getProperty("user.name"));
    }

    public String getUserName()
    {
        return userName;
    }

   
    private void setUserName(String userName)
    {
        this.userName = userName;
    }

    /**
     * Initialise selenium driver
     * 
     * @throws Exception
     */
    @BeforeSuite(alwaysRun = true)
    public void initialSetUp() throws Exception
    {
        logger.info("Initialize webdriver and login details");
        initialization();
        getDriver().get(getUrl());
        readProperties();
    }

    @BeforeMethod(alwaysRun = true)
    public void showStartInfo(Method method)
    {
        logger.info("*** START TestNG Method: {" + method.getName() + "} ***");
    }

    @AfterMethod(alwaysRun = true)
    public void showStartInfo(ITestResult method)
    {
        logger.info("*** END TestNG Method:   {" + method.getMethod().getMethodName() + "} ***");
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown()
    {
        if(getDriver() != null)
        {
            getDriver().quit();
        }
    }
}
