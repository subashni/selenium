package org.sage.people.utils;

public class ExceptionBase extends Exception
{
    private static final long serialVersionUID = -8500644323000193749L;
    public ExceptionBase()
    {
    }

    public ExceptionBase(String message)
    {
        super(message);
    }

    public ExceptionBase(Throwable cause)
    {
        super(cause);
    }

    public ExceptionBase(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ExceptionBase(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
