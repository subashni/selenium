package org.sage.people.utils;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.sage.people.base.TestBase;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

public class ExceptionListener extends TestListenerAdapter
{
    public void takeScreenShotOnFailure(ITestResult testResult) throws Exception 
    { 
        WebDriver driver = TestBase.getDriver();
            if (testResult.getStatus() == ITestResult.FAILURE) 
            { 
                    File  scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
                    File copFile = new File("errorScreenshots\\" + testResult.getName() +   ".jpg");
                    FileUtils.copyFile(scrFile, copFile);
            } 
                    
    }
    
}

