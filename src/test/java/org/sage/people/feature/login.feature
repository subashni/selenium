Feature: Salesforce login feature

# Invalid login 
Scenario: Invalid login Test 
Given user is in the salesforce login page
Then user enters "test@test.com" and "test"
Then user click on login button 
Then validate error message is displayed
Then  quit browser

# Valid login details
Scenario: Valid login Test
Given user is in the salesforce login page
Then user enters username and password
Then user click on login button
Then validate is successful logged in
Then quit browser 